## Introduction


Data comes in many forms and formats, however, it does not readily display information or patterns that can be consumed by recipients of this data. A Data Scientists’ job is to extract viable information and to tell a story using this data. In this assignment, you will analyze climate data collected for the city of Chicago in the month of March 2014, and use Python programming concepts to display the results in a dashboard.

There are many climatic parameters that are interrelated. You will investigate relevant parameters to understand the correlation between humidity, and wind speed on temperature and visibility at sunrise and sunset.

In this final assignment, you will create a Jupyter Notebook using IBM Watson Studio with Python. 

## Author(s)
<h4> Joseph Santarcangelo <h4/>


## Change log
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-09-05 | 2.0 | Malika Singla | Migrated Lab to Markdown |



## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>

## Assess your own response

When you have completed the required number of peer assessments in your [ORA assignment](https://gitlab.com/MalikaSingla/projects_sko_team/-/blob/master/SKO_mdfiles/Python%20for%20data%20Science/finalexam_ORA.md), the self assessment step of the assignment becomes available. You see your response along with the same rubric that you used in the peer assessment step.

Perform an assessment of your own response, and then select Submit Your Assessment.

When you have completed assessing your own response, the next step in the assignment becomes available. If there are no further steps, and if you have received the required number of peer assessments on your own response, you can receive your score.
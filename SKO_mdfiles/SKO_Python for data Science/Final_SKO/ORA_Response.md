## Submit your response

Submitting your response is usually the first step in an [Open Response Assessment (ORA) assignment](https://gitlab.com/MalikaSingla/projects_sko_team/-/blob/master/SKO_mdfiles/Python%20for%20data%20Science/finalexam_ORA.md).

### To submit your response to an Open Response Assessment question
1. **Read each question carefully:** Some course teams include important information in the question, such as how long a response must be, or specific topics that your response must cover. Note that the total word count for your response cannot be more than 10,000 words.
2. **Enter your response:** For each question, enter your response into the field under Your Response. In some assignments, you can submit images or other types of files along with or instead of a written response. If available, Browse and Upload your files options appear below the response field. For information about uploading images or other files in your ORA assignment, see Submit a File with Your Response.
3. When you have finished answering all of the questions, select **Submit your response** and move to the next step.
4. If you need more time, you can select **Save Your Progress** to save a draft of your responses, and then come back and submit them later.

`Note: For assignments that require LaTeX responses, a Preview in LaTeX option is available that you can use to preview your work before you submit your response.`


After you submit your response, the next step, which is usually either assessment training or peer assessment, becomes available. However, you do not have to start the next step right away. If you want to stop working and come back later, just refresh or reopen your browser when you come back.

For more information about the steps in an ORA assignment, see Steps in an [ORA assignment](https://gitlab.com/MalikaSingla/projects_sko_team/-/blob/master/SKO_mdfiles/Python%20for%20data%20Science/finalexam_ORA.md).
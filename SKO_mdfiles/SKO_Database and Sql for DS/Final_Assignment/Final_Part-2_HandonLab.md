## LAB: Execute the SQL Queries for Final Assignment

### Problems

Now write and execute SQL queries to solve assignment problems.

**Problem 1**

Find the total number of job openings recorded in the NYC Jobs table.

**Problem 2**

Retrieve first 10 rows from the CITY PAYROLL table.

**Problem 3**

Determine how many Job Openings exist for the Agency NYC Housing Authority.

**Problem 4**

How many unique types of Title Descriptions have been recorded at the Richmond work location borough?

Did you know? IBM Watson Studio lets you build and deploy an AI solution, using the best of open source and IBM software and giving your team a single environment to work in. [Learn more here](https://cloud.ibm.com/catalog/services/watson-studio).

**Problem 5**

In the CITY PAYROLL table, list all employees whose last names start with the letter ‘E’. Show the First Name, Last Name, and Title Description.

**Problem 6**

List the titles of job openings with a salary between $16 and $21/hr in the Dept of Info Tech and Telecom for students.

**Problem 7**

What is the average homeless population in Queens?

**Problem 8**

List the top 5 agencies by average salary.

**Problem 9**

Using a sub-query, which area had the lowest estimate for homeless population in 2012?

**Problem 10**

_Without using an explicit JOIN operator_

How many job openings for CARETAKER have a higher salary than the highest current CARETAKER salary?

Copyright © 2018 [cognitiveclass.ai](https://e-7778a80cd1.cognitiveclass.ai/files/labs/DB0201EN/cognitiveclass.ai%3Futm_source%3Dbducopyrightlink%26utm_medium%3Ddswb%26utm_campaign%3Dbdu?_xsrf=2%7C2a202641%7C267bae38254ce548f8cb7793612e85be%7C1579545132). This notebook and its source code are released under the terms of the [MIT License](https://bigdatauniversity.com/mit-license/).

**Download Jupyter Notebook**

To view the notebook in your own Jupyter environment, download the Jupyter notebook (IPYNB file) by clicking on the link below:

[Jupyter notebook](https://courses.myclass.skillup.online/assets/courseware/v1/e53a2a582e35c63fc6129656ea3e5469/asset-v1:IBM+DB0201EN-Skillup+2020+type@asset+block/DB0201EN-Week4-2-2-PeerAssign-v5-py-EDX.ipynb)

## Author(s)
<h4> Rav Ahuja <h4/>


## Change log
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-09-05 | 2.0 | Malika Singla | Migrated Lab to Markdown |



## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>

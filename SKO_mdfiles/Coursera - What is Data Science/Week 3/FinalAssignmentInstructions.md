## Instructions

In this Assignment, you will demonstrate your understanding of the videos and the readings by answering open-ended questions, defining data science and data scientist, and describing the different sections comprising a final deliverable of a data science project. Please note that this assignment is worth 10% of your final grade.


### Review criteria
This assignment will be graded by your peers who are also completing this course during the same session.
**Course Text Book: ‘Getting Started with Data Science’ Publisher: IBM Press; 1 edition (Dec 13 2015) Print.**

**Author: Murtaza Haider**

![image](images/Book.PNG)

Prescribed Reading: Chapter 7 Pg. 235-236

![image](images/book_page235.png)

![image](images/book_page236.png)
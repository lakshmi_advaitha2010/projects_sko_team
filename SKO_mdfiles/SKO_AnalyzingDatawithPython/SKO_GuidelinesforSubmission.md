## Guidelines for Submission

1. Create your Jupyter Notebook on [IBM Watson Studio](https://cloud.ibm.com/catalog/services/watson-studio). To learn how to create a Notebook on IBM Watson Studio, follow the [steps in this guide](./IBM_Watson_Setup.md). It also includes the link to the Notebook you will use to answer the questions in this assignment.

2. Ensure you have generated an output for the code you have written for each question, and then clicked **Save version** under the File menu in IBM Watson.

3. Your notebook must be shareable in order to be graded. See instructions in the first question on how to make the notebook shareable.

**If you do not make your link shareable, you cannot be graded on the remainder of this assignment. (2 marks)**

Follow these steps to make the Notebook shareable:

1. After you complete your notebook, you will have to share it to be evaluated by your peers. Select the icon on the top right highlighted in Red in the image below. This will bring up a dialogue box offering various sharing options. Select the option **All content excluding sensitive code cells**.

![image](images/share_notebook.png)

2. Scroll down and select the link to the Notebook:

![image](images/url_notebook.png)

3. Submit the URL of your publicly shared notebook on IBM Watson Studio.

## Author(s)
<h4> Joseph Santarcangelo <h4/>


## Change log
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-09-05 | 2.0 | Malika Singla | Updated the screenshot & Migrated Lab to Markdown |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>


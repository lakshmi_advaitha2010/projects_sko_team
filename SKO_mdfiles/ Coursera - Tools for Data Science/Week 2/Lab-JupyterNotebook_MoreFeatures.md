## Lab - Jupyter Notebook - More Features

**This lab is a continuation of the previous lab.**

**Note:** If you have JupyterLab on Skills Network Labs open in a tab already, continue using that tab. Otherwise, click on _Open Tool_ button below to launch Jupyter Notebooks.

**Exercise 5 - Rename your Notebook**

1. In the list of notebooks in the right-hand panel, click on the arrow (>) to the left of the notebook name to expand the list of options.

2. Click on _Rename_ to rename your notebook to something like _My_Notebook.ipynb_.


**Exercise 6 - Save and Download your Jupyter Notebook from Skills Network Labs to your computer**

Although your notebooks and data is preserved in your account after signing out, sometimes you may want to download your notebook to your computer.

- To save, click on the Save button in the menu, or go to _Save Notebook_.

- To download the notebook, click on the Files pane to open the list of files. Navigate to your notebook, and right-click on the file to download the notebook.


**Exercise 7 - Upload a Jupyter Notebook to Skills Network Labs**

- To upload a Jupyter Notebook from your computer to Skills Network Labs, drag and drop a .ipynb file from your computer directly into the browser. You can use the notebook you downloaded from Exercise 6.

- Your notebook should open up automatically once it has finished uploading.


**Exercise 8 - Change your kernel (to Python 3, R) in JupyterLab**

With a notebook open, click on the kernel name (e.g., Python 3) in the top-righthand corner of the notebook to open up a pop-up window to change it to a different language.

**Passed** 100%

This course uses a third-party tool, Lab - Jupyter Notebook - More Features, to enhance your learning experience. The tool will reference basic information like your name, email, and Coursera ID.

[Open Tool](https://labs.cognitiveclass.ai/tools/jupyterlab/?lti=true)

## Lab: RStudio – The Basics  

This lab introduces you to R and RStudio. First, we need to install it. 

Please install R appropriate for your operating system. You can obtain it here: https://cran.rstudio.com/

Once you’ve installed R, please download and install RStudio appropriate for your operating system. You can find it here: https://rstudio.com/products/rstudio/download/#download

1. Start RStudio. You should see something like this:

![image](images/01_R.png)

2. Now click in the tiny “plus” symbol top left.

![image](images/02_R.png)

and select “Rscript” . This gives you the following:

![image](images/03_R.png)

3. Now we load the iris data set which you already should be familiar with from the previous labs. Please enter the following lines into the editor window which just appeared.

`library (datasets)`

`data(iris)`

`View(iris) `

Then select them all such that they turn blue. Then click on the tiny run icon just above the editor window.

![image](images/04_R.png)

4. You are directly taken to the data view tab to inspect your data set:

![image](images/05_R.png)

5. We can see that there are five columns in this data set and that the first four are floating point and the last one is a label of data type string, which contains the category value of our data set. We also see that we have 150 entries in total of which we are seeing the first 19. Now we want to know how many different species there are present in the data set. Therefore, please type the following command into the editor window:

`unique(iris$Species)`

and click the run icon:

![image](images/06_R.png)

![image](images/07_R.png)

6. In the Console window at the bottom you’ll see the result of the executed command and will know that there are only three different species present in the data set. Now it’s time to look into the data set in more detail. We’ll create a similar plot as we’ve already done in the JupyterLab. To do so we first need to install the ggplot2 package. Please type and execute the following command:

`install.packages(c("GGally", "ggplot2"))`

7. Now that you have installed the libraries necessary to create some nice plots let’s execute the following commands:

`library(GGally)`

`ggpairs(iris, mapping=ggplot2::aes(colour = Species))`

8. You’ll now see the following plot in the Plots window:

![image](images/08_R.png)

9. This gives us a lot of information for a single line of code. First, we see the data distributions per column and species on the diagonal. Then we see  all pair-wise scatter plots on the tiles left to the diagonal, again broken down by color. It is, for example, obvious to see that a line can be drawn to separate _setosa_ against _versicolor_ and _virginica_. In later courses, we will of course teach how the overlapping species can be separated as well. This is called supervised machine learning using non-linear classifiers by the way. Then you see the correlation between individual columns in the tiles right to the diagonal which confirms our thoughts that _setose_ is more different,  hence more easy to distinguish, than _versicolor_ and _virginica_ since a correlation value close to one signifies high similarity, whereas a value closer to zero signifies less similarity. The remaining plots on the right are called _box-plots_ and the ones at the bottom are called _histograms_, but we won’t go into detail here, rather we will save this for a more advanced course in this series.

This concludes the lab, I hope you’ve enjoyed it! 


